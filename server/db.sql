DROP TABLE IF EXISTS `employees`;

CREATE TABLE IF NOT EXISTS `employees` (
  `id` INT NOT NULL,
  `name` VARCHAR(256) NOT NULL,
  `email` VARCHAR(256) NOT NULL,
  `password` VARCHAR(256) NOT NULL,
  `manager_id` INT NULL,
  `leave_allowance` INT NOT NULL,
  `leave_remaining` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));


DROP TABLE IF EXISTS `leave`;

CREATE TABLE IF NOT EXISTS `leave` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `employee_id` INT NOT NULL,
  `start_date` DATETIME NOT NULL,
  `end_date` DATETIME NOT NULL,
  `days` INT NOT NULL,
  `notes` TEXT NULL,
  `status` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));
