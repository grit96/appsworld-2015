var mysql = require('mysql');
var config = require('./config');

module.exports = (function() {
  var db = {};
  var connection = mysql.createConnection({
      host: 'localhost',
      user: config.db_user,
      password: config.db_pass,
      database: config.db_name
  });

  connection.connect(function(err) {
    if (err) console.error(err);
  });

  function db_handler(cb) {
    return function(err, result) {
      if (err) console.error(err);
      cb(err, result);
    };
  }

  db.get_user = function(employee_id, cb) {
    var sql = 'SELECT * FROM `employees` WHERE `id` = ? LIMIT 1';
    connection.query(sql, [employee_id], db_handler(cb));
  };

  db.get_users = function(manager_id, cb) {
    var sql = 'SELECT * FROM `employees` WHERE `manager_id` = ?';
    connection.query(sql, [manager_id], db_handler(cb));
  };

  db.create_user = function(employee_id, name, leave_allowance, leave_remaining, manager_id, email, password, cb) {
    var sql = 'INSERT INTO `employees` VALUES (?, ?, ?, ?, ?, ?, ?)';
    connection.query(sql, [employee_id, name, email, password, manager_id, leave_allowance, leave_remaining], db_handler(cb));
  };

  db.create_leave_request = function(employee_id, start_date, end_date, days, notes, cb) {
    var sql = 'INSERT INTO `leave` VALUES (0, ?, ?, ?, ?, ?, ?)';
    connection.query(sql, [employee_id, start_date, end_date, days, notes, 0], db_handler(cb));
  };

  db.modify_leave_request = function(leave_id, start_date, end_date, days, notes, status, cb) {
    var sql = 'UPDATE `leave` SET `start_date` = ?, `end_date` = ?, `days` = ?, `notes` = ?, `status` = ? WHERE `id` = ?';
    connection.query(sql, [start_date, end_date, days, notes, status, leave_id], db_handler(cb));
  };

  db.decrease_employee_leave = function(employee_id, days, cb) {
    var sql = 'UPDATE `employees` SET `leave_remaining` = `leave_remaining` - ? WHERE `id` = ?';
    connection.query(sql, [days, employee_id], db_handler(cb));
  };

  db.increase_employee_leave = function(employee_id, days, cb) {
    var sql = 'UPDATE `employees` SET `leave_remaining` = `leave_remaining` + ? WHERE `id` = ?';
    connection.query(sql, [days, employee_id], db_handler(cb));
  };

  db.delete_leave_request = function(leave_id, cb) {
    var sql = 'DELETE FROM `leave` WHERE `id` = ?';
    connection.query(sql, [leave_id], db_handler(cb));
  };

  db.get_leave_request = function(leave_id, cb) {
    var sql = 'SELECT * FROM `leave` WHERE `id` = ? LIMIT 1';
    connection.query(sql, [leave_id], db_handler(cb));
  };

  db.get_leave_for_user = function(employee_id, cb) {
    var sql = 'SELECT * FROM `leave` WHERE `employee_id` = ?';
    connection.query(sql, [employee_id], db_handler(cb));
  };

  db.get_manager = function(employee_id, cb) {
    var sql =
      'SELECT `manager`.* ' +
        'FROM `employees` `employee` ' +
      'JOIN `employees` `manager` ' +
        'ON `employee`.`manager_id` = `manager`.`id` ' +
      'WHERE `employee`.`id` = ? LIMIT 1';

    connection.query(sql, [employee_id], db_handler(cb));
  };

  return db;

}());
