var express = require('express'),
    bcrypt = require('bcrypt'),
    bodyParser = require('body-parser'),
    basicAuth = require('basic-auth'),
    nodemailer = require('nodemailer'),
    icalendar = require('icalendar'),
    config = require('./config'),
    db = require('./db');


require('string-format').extend(String.prototype);


var transporter = nodemailer.createTransport({
  service: 'Gmail',
  auth: {
    user: config.email_user,
    pass: config.email_pass
  }
});


var app = express();

app.set('port', process.env.PORT || 5005);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());


function auth(req, res, next) {
  function unauthorized(res) {
    res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
    return res.send(401);
  }

  var user = basicAuth(req);

  if (!user || !user.name || !user.pass) {
    return unauthorized(res);
  }

  check_user(user.name, user.pass, function(outcome) {
    outcome ? next() : unauthorized(res);
  });
}

function check_user(employee_id, password, cb) {
  db.get_user(employee_id, function(err, result) {
    var outcome;

    if (result.length === 1) {
      var hash = result[0].password;
      outcome = bcrypt.compareSync(password, hash);
    }

    cb(outcome);
  });
}


app.use(function logger(req, res, next) {
  console.log('%s %s %s', req.method, req.url, req.path);
  next();
});

app.use(function allow_cors(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  next();
});

app.get('/', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.post('/api/login', function(req, res) {
  var employee_id = parseInt(req.body.employee_id),
      password = req.body.password;

  check_user(employee_id, password, function(outcome) {
    if (!outcome) return res.status(400).send({err: 'Login failed'});

    res.send({success: outcome});
  });
});

app.post('/api/get_user', auth, function(req, res) {
  var employee_id = parseInt(req.body.employee_id);

  db.get_user(employee_id, function(err, result) {
    if (!result.length) return res.status(400).send({err: 'User not found'});

    res.send({data: result});
  });
});

app.post('/api/get_user_leave', auth, function(req, res) {
  var employee_id = parseInt(req.body.employee_id);

  db.get_leave_for_user(employee_id, function(err, result) {
    if (err) return res.status(400).send({err: 'Get leave request failed'});
    res.send({success: result});
  });
});

app.post('/api/get_leave_request', auth, function(req, res) {
  var leave_id = parseInt(req.body.leave_id);

  db.get_leave_request(leave_id, function(err, result) {
    if (err) return res.status(400).send({err: 'Get leave request failed'});
    res.send({success: result});
  });
});

app.post('/api/create_user', auth, function(req, res) {
  var employee_id = parseInt(req.body.employee_id),
      name = req.body.name,
      leave_allowance = parseInt(req.body.leave_allowance),
      leave_remaining = parseInt(leave_allowance),
      manager_id = parseInt(req.body.manager_id) || null,
      email = req.body.email,
      password = req.body.password;

  var hash = bcrypt.hashSync(password, 12);

  db.create_user(employee_id, name, leave_allowance, leave_remaining, manager_id, email, hash, function(err, result) {
    if (err) return res.status(400).send({err: 'Create user failed'});

    res.send({success: true});
  });
});

// Need an isManager API call

app.post('/api/create_leave_request', auth, function(req, res) {
  var employee_id = parseInt(req.body.employee_id),
      start_date = new Date(parseInt(req.body.start_date)),
      end_date = new Date(parseInt(req.body.end_date)),
      days = parseInt(req.body.days),
      notes = req.body.notes || null;

  db.create_leave_request(employee_id, start_date, end_date, days, notes, function(err, result) {
    var leave_id = result.insertId;
    if (err) return res.status(400).send({err: 'Leave create request failed'});
    db.decrease_employee_leave(employee_id, days, function(err, result) {
      db.get_manager(employee_id, function(err, result) {
        if (!result.length) return console.warn('Manager not found');

        var manager = result[0];
        db.get_user(employee_id, function(err, result) {
          if (!result.length) return console.warn('Employee not found');

          var employee = result[0];
          transporter.sendMail({
            to: manager.email,
            cc: employee.email,
            subject: 'Leave Request {}'.format(leave_id),
            text: 'Hello, {} has a requested leave for {} days between {} and {}'.format(employee.name, days, start_date, end_date),
          }, function() {
            res.send({success: true});
          });
        });
      });
    });
  });
});

app.post('/api/delete_leave_request', auth, function(req, res) {
  var leave_id = parseInt(req.body.leave_id),
      employee_id = parseInt(req.body.employee_id),
      days = parseInt(req.body.days);

  db.delete_leave_request(leave_id, function(err, result) {
    if (err) return res.status(400).send({err: 'Leave deletion request failed'});
    db.increase_employee_leave(employee_id, days, function(err, result) {
      // DO STUFF
      res.send({success: true});
    });
  });
});

app.post('/api/modify_leave_request', auth, function(req, res) {
  var leave_id = parseInt(req.body.leave_id),
      employee_id = parseInt(req.body.employee_id),
      start_date = new Date(parseInt(req.body.start_date)),
      end_date = new Date(parseInt(req.body.end_date)),
      old_days = parseInt(req.body.old_days),
      new_days = parseInt(req.body.new_days),
      status = parseInt(req.body.status),
      notes = req.body.notes || null;

  db.modify_leave_request(leave_id, start_date, end_date, new_days, notes, status, function(err, result) {
    if (err) return res.status(400).send({err: 'Leave update request failed'});
    if (new_days > old_days) {
      db.decrease_employee_leave(employee_id, new_days - old_days, function(err, result) {
        // DO STUFF
        res.send({success: true});
      });
    } else {
      db.increase_employee_leave(employee_id, old_days - new_days, function(err, result) {
        // DO STUFF
        res.send({success: true});
      });
    }
  });
});

app.post('/api/approve_leave_request', auth, function(req, res) {
  var leave_id = parseInt(req.body.leave_id);

  update_leave_status(leave_id, 1, function(l, e) {
    var event = new icalendar.VEvent();
    event.setSummary('Leave Request for {} days'.format(l.days));
    event.setDate(new Date(l.start_date), new Date(l.end_date));

    transporter.sendMail({
      to: e.email,
      subject: 'Leave Request Approved {}'.format(l.id),
      text: 'Hello {}, your leave for {} days between {} and {} has been approved'.format(e.name, l.days, l.start_date, l.end_date),
      alternatives: [{
        filename: 'leave.ics',
        contentType: 'text/calendar',
        content: new Buffer(event.toString())
      }]
    }, function() {
      res.send({success: true});
    });
  });
});

app.post('/api/reject_leave_request', auth, function(req, res) {
  var leave_id = parseInt(req.body.leave_id);

  update_leave_status(leave_id, 2, function(l, e) {
    transporter.sendMail({
      to: e.email,
      subject: 'Leave Request Rejected {}'.format(l.id),
      text: 'Hello {}, your leave for {} days between {} and {} has been rejected'.format(e.name, l.days, l.start_date, l.end_date)
    }, function() {
      res.send({success: true});
    });
  });
});

function update_leave_status(leave_id, status, cb) {
  db.get_leave_request(leave_id, function(err, result) {
    if (!result.length) return res.status(400).send({err: 'Leave not found'});

    var l = result[0];
    db.modify_leave_request(l.id, l.start_date, l.end_date, l.days, l.notes, status, function(err, result) {
      if (err) return res.status(400).send({err: 'Leave update request failed'});

      db.get_user(l.employee_id, function(err, result) {
        if (!result.length) return console.warn('Employee not found');

        cb(l, result[0]);
      });
    });
  });
}


app.listen(app.get('port'));
console.log('Express server listening on port', app.get('port'));
